local S = minetest.get_translator("decoblocks")



minetest.register_node("decoblocks:beaten_path", {
  description = S("Beaten path"),
  tiles = {"decoblocks_beatenpath.png"},
  drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.4375, 0.5}
		}
	},
  groups = {crumbly = 3},
  sounds = default.node_sound_dirt_defaults(),
})


minetest.register_node("decoblocks:cobweb", {
  description = S("Cobweb"),
  drawtype = "signlike",
  tiles = {"decoblocks_cobweb.png"},
  inventory_image = "decoblocks_cobweb.png",
  wield_image = "decoblocks_cobweb.png",
  walkable = false,
  paramtype = "light",
	paramtype2 = "wallmounted",
  sunlight_propagates = true,
  selection_box = {type = "wallmounted"},
  groups = {snappy=3, flammable=3},
  sounds = default.node_sound_leaves_defaults(),

  drop = "decoblocks:string"
})



minetest.register_craftitem("decoblocks:string", {
  description = S("String"),
  inventory_image = "decoblocks_string.png"
})






minetest.register_alias("beaten_path", "decoblocks:beaten_path")


-- crops melon
minetest.register_node("decoblocks:melon", {
	description = "Melon",
	tiles = {
		"crops_melon_top.png",
		"crops_melon_bottom.png",
		"crops_melon.png",
	},
	sunlight_propagates = false,
	use_texture_alpha = false,
	walkable = true,
	groups = { snappy=3, flammable=3, oddly_breakable_by_hand=2 },
	paramtype2 = "facedir",
	sounds = default.node_sound_wood_defaults({
		dig = { name = "default_dig_oddly_breakable_by_hand" },
		dug = { name = "default_dig_choppy" }
	}),
})

