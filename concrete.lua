local S = minetest.get_translator("decoblocks")



local function register_concrete(name, desc)

  name = "concrete_" .. name
  desc = S(desc)

  minetest.register_node("decoblocks:" .. name, {
  	description = desc,
  	tiles = {
  		"decoblocks_" .. name .. ".png",
  	},
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults()
  })

  minetest.register_node("decoblocks:" .. name .. "_slab", {
  	description = desc .. " " .. S("(slab)"),
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
  	tiles = {
  		"decoblocks_" .. name .. ".png",
  	},
    node_box = {
  		type = "fixed",
  		fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults(),

    on_place = function(itemstack, placer, pointed_thing)
      decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
    end,
  })

  minetest.register_node("decoblocks:" .. name .. "_stair", {
		description = desc .. " " .. S("(stair)"),
		drawtype = "nodebox",
		paramtype = "light",
		paramtype2 = "facedir",
    tiles = {
  		"decoblocks_" .. name .. ".png",
  	},
    node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, 0.0, 0.5},
				{-0.5, 0.0, 0.0, 0.5, 0.5, 0.5},
			},
		},
		groups = {cracky = 3},
		sounds = default.node_sound_stone_defaults(),

		on_place = function(itemstack, placer, pointed_thing)
			if pointed_thing.type ~= "node" then
				return itemstack
			end

			return decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
		end,
	})

  minetest.register_alias(name, "decoblocks:" .. name)
  minetest.register_alias(name .. "_slab", "decoblocks:" .. name .. "_slab")
  minetest.register_alias(name .. "_stair", "decoblocks:" .. name .. "_stair")

end

register_concrete("blue", "Blue concrete")
register_concrete("red", "Red concrete")
register_concrete("grey", "Grey concrete")
register_concrete("light_grey", "Light grey concrete")
register_concrete("dark_grey", "Dark grey concrete")
register_concrete("black", "Black concrete")
