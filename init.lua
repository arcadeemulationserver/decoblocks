decoblocks = {}

dofile(minetest.get_modpath("decoblocks") .. "/carpets.lua")
dofile(minetest.get_modpath("decoblocks") .. "/concrete.lua")
dofile(minetest.get_modpath("decoblocks") .. "/furniture.lua")
dofile(minetest.get_modpath("decoblocks") .. "/industrial.lua")
dofile(minetest.get_modpath("decoblocks") .. "/nature.lua")
dofile(minetest.get_modpath("decoblocks") .. "/utils.lua")
dofile(minetest.get_modpath("decoblocks") .. "/_block_league/hyperium.lua")
dofile(minetest.get_modpath("decoblocks") .. "/_block_league/neden1.lua")
dofile(minetest.get_modpath("decoblocks") .. "/_block_league/tunnel.lua")
