--
-- 1-2: tetto
--
local function register_neden1_roof()

  local n = 1

  minetest.register_node("decoblocks:blockleague_neden1_" .. n, {
    description = "[BL] Neden1 " .. n,
    tiles = {
      "decoblocks_blockleague_neden1_" .. n .. ".png"
    },
    paramtype2 = "facedir",
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults()
  })

  local n2 = n + 1

  minetest.register_node("decoblocks:blockleague_neden1_" .. n2, {
    description = "[BL] Neden1 " .. n2,
    drawtype = "nodebox",
    tiles = {
      "decoblocks_blockleague_neden1_" .. n .. ".png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    paramtype2 = "facedir",
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults(),

    on_place = function(itemstack, placer, pointed_thing)
      decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
    end,
  })

end



register_neden1_roof()



--
-- 3: terreno un po' più basso
--
minetest.register_node("decoblocks:blockleague_neden1_3", {
  description = "[BL] Neden1 3",
  tiles = {"default_sand.png"},
  drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.3, 0.5}
		}
	},
  groups = {crumbly = 3},
  sounds = default.node_sound_sand_defaults()
})



--
-- 4: acqua sporca
--
minetest.register_node("decoblocks:blockleague_neden1_4", {
	description = "[BL] Neden1 4",
	drawtype = "nodebox",
	tiles = {"decoblocks_blockleague_neden1_4.png"},
  node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.3, 0.5}
		}
	},
	use_texture_alpha = true,
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	post_effect_color = {a = 103, r = 90, g = 60, b = 30},

	groups = {water = 3, liquid = 3, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
})


--
-- 4: rete a maglia alta metà
--
minetest.register_node("decoblocks:blockleague_neden1_5", {
    description = "[BL] Neden1 5",
    drawtype = "nodebox",
    tiles = {"decoblocks_chainlink_fence.png"},
    inventory_image = "decoblocks_chainlink_fence.png",
    wield_image = "decoblocks_chainlink_fence.png",
    paramtype = "light",
    sunlight_propagates = true,
    node_box = {
  		type = "fixed",
  		fixed = {
  			{-0.5, -0.2, -0.5, 0.5, 0.0, 0.5}
  		}
  	},
    groups = {cracky = 3},
    sounds = default.node_sound_metal_defaults()
})
