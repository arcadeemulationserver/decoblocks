local function register_tunnel_block(n)

  minetest.register_node("decoblocks:blockleague_tunnel_" .. n, {
    description = "[BL] Tunnel " .. n,
    tiles = {
      "decoblocks_blockleague_tunnel_" .. n .. ".png",
      "decoblocks_concrete_blue.png"
    },
    paramtype2 = "facedir",
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults()
  })

  local n2 = n + 3

  minetest.register_node("decoblocks:blockleague_tunnel_" .. n2, {
    description = "[BL] Tunnel " .. n2,
    drawtype = "nodebox",
    tiles = {
      "decoblocks_blockleague_tunnel_" .. n .. ".png",
      "decoblocks_concrete_blue.png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    paramtype2 = "facedir",
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults()
  })

end



register_tunnel_block(1)
register_tunnel_block(2)
register_tunnel_block(3)
