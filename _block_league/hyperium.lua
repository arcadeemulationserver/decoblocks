local function register_hyperium_block(n)

	minetest.register_node("decoblocks:blockleague_hyperium_" .. n, {
    description = "[BL] Hyperium " .. n,
    drawtype = "nodebox",
    tiles = {
      "decoblocks_blockleague_hyperium_" .. n .. ".png",
      "decoblocks_concrete_black.png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    paramtype = "light",
    paramtype2 = "facedir",
    light_source = default.LIGHT_MAX/2,
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults(),

    on_place = function(itemstack, placer, pointed_thing)
      decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
    end,
  })

end

register_hyperium_block(1)
register_hyperium_block(2)
