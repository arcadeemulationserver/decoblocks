# A.E.S. - Decoblocks

Decoration blocks scattered all over the server

Images under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
Except:

crops melon:
code (in nature.lua)
(C) Auke Kok <sofar@foo-projects.org>
	LGPL-2.0+

texture (modified):
(C) Auke Kok <sofar@foo-projects.org>
	CC-BY-SA-3.0
